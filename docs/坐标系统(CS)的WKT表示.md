## WKT 格式
> EPSG:4326坐标系的WKT表示, 详情参考[OGC说明](http://docs.opengeospatial.org/is/18-010r7/18-010r7.html)

```
<!-- 定义地理坐标系，名称为WGS 84 -->
GEOGCRS["WGS 84",
    <!-- 定义基准面，名称为World Geodetic System 1984 -->
    DATUM["World Geodetic System 1984",
        <!-- 定义椭球，名称为WGS 84, 长半径为6378137，反曲率为298.257223563，长度单位为1米 -->
        ELLIPSOID["WGS 84",6378137,298.257223563,
            LENGTHUNIT["metre",1]]],
    <!-- 定义中央经线，名称为Greenwich, 值为0，角度单位为0.0174532925199433度 -->
    PRIMEM["Greenwich",0,
        ANGLEUNIT["degree",0.0174532925199433]],
    <!-- 定义坐标系统，椭球坐标系，二维 -->
    CS[ellipsoidal,2],
        <!-- 定义轴 geodetic latitude (Lat)， 朝北，第一个，角度单位为0.0174-->
        AXIS["geodetic latitude (Lat)",north,
            ORDER[1],
            ANGLEUNIT["degree",0.0174532925199433]],
        <!-- 定义轴 geodetic longitude (Lon)， 朝东，第二个，角度单位为0.0174-->
        AXIS["geodetic longitude (Lon)",east,
            ORDER[2],
            ANGLEUNIT["degree",0.0174532925199433]],
    <!-- 定义用法 -->
    USAGE[
        <!-- 目的说明 -->
        SCOPE["unknown"],
        <!-- 范围说明 -->
        AREA["World"],
        <!-- 范围定义 -->
        BBOX[-90,-180,90,180]],
    <!-- 定义ID -->
    ID["EPSG",4326]]
```

# Proj4
> Proj4中表示坐标系使用proj-strings，下面给出QGIS中EPSG:4326坐标系的表示，具体参数[参考](https://proj.org/usage/projections.html)

```
<!-- +proj：投影名称 -->
<!-- +a 长半轴长度 -->
<!-- +b 短半轴长度 -->
<!-- +lat_ts 纬度真实比例 -->
<!-- +lon_0 经度中心 -->
<!-- +x_0 东偏 -->
<!-- +y_0 北偏 -->
<!-- +k 比例参数 -->
<!-- +units 单位 -->
<!-- +nadgrids 用于基准转换的格网文件(.gsb) -->


+proj=merc +a=6378137 +b=6378137 +lat_ts=0 +lon_0=0 +x_0=0 +y_0=0 +k=1 +units=m +nadgrids=@null +wktext +no_defs
```
