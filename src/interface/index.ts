export interface Param4 {
  DX: number;
  DY: number;
  T: number;
  K: number;
}
