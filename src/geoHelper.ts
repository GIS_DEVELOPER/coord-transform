import Proj, { InterfaceProjection } from "proj4";

export class ProjHelper {
  private localProj: InterfaceProjection;
  private beijing54Proj: InterfaceProjection;
  private wgs84Proj: InterfaceProjection;
  constructor() {
    Proj.defs(
      "EPSG:4214",
      "+proj=longlat +ellps=krass +towgs84=15.8,-154.4,-82.3,0,0,0,0 +no_defs"
    );
    Proj.defs(
      "EPSG:42141",
      "+proj=tmerc +lat_0=0 +lon_0=0 +k=0.986127400943 +x_0=3115001.350288 +y_0=852553.370440 +ellps=krass +units=m +no_defs"
    );

    this.localProj = Proj.Proj("EPSG:42141");
    this.wgs84Proj = Proj.Proj("EPSG:4326");
    this.beijing54Proj = Proj.Proj("EPSG:4214");
  }

  public bj542wgs(coord: [number, number]) {
    if (!coord) throw new Error("coordinate error!");

    return Proj.transform(this.beijing54Proj, this.wgs84Proj, coord);
  }

  public wgs2bj54(coord: [number, number]) {
    if (!coord) throw new Error("coordinate error!");

    return Proj.transform(this.wgs84Proj, this.beijing54Proj, coord);
  }

  public local2bj54(coord: [number, number]) {
    if (!coord) throw new Error("coordinate error!");

    return Proj.transform(this.localProj, this.beijing54Proj, coord);
  }

  public bj542local(coord: [number, number]) {
    if (!coord) throw new Error("coordinate error!");

    return Proj.transform(this.beijing54Proj, this.localProj, coord);
  }

  public local2wgs(coord: [number, number]) {
    if (!coord) throw new Error("coordinate error!");

    return Proj.transform(this.localProj, this.wgs84Proj, coord);
  }

  public wgs2local(coord: [number, number]) {
    if (!coord) throw new Error("coordinate error!");

    return Proj.transform(this.wgs84Proj, this.localProj, coord);
  }
}
