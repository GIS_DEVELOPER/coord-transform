import { UIHelper } from "./uiHelper";
import {
  localproj_bjproj,
  bjproj_bjlnglat,
  bjlnglat_wgslnglat,
} from "./projHelper";

let uiHelper = new UIHelper();

let inputCoord = uiHelper.addInput();
inputCoord.value = "328443.4399,345557.6179";
let btn1 = uiHelper.addBtn("localProj_bjProj");
uiHelper.addBr();
let resLabel = uiHelper.addLabel();

btn1.addEventListener("click", (e) => {
  let value = inputCoord.value;
  if (!value) throw new Error("input coordinates error");

  let sourceCoord = value.split(",").map((c) => parseFloat(c));
  sourceCoord = [
    [328443.4399, 345557.6179],
    [328573.7734, 345796.7321],
    [328521.471, 345883.2094],
    [328462.7426, 345818.7423],
  ] as any;
  // local proj => beijing54 proj
  let interCooord = localproj_bjproj(sourceCoord as any, {
    x_0: 3109444.007744,
    y_0: 557065.040376,
    t: -1.188967233,
    k: 0.985971406835,
  });

  // beijing54 proj => beijing54 geos
  let resCoord0 = bjproj_bjlnglat(interCooord as any, {
    lon_0: 120,
    x_0: 500000,
  });
  let resCoord = bjlnglat_wgslnglat(resCoord0 as any);
  resCoord.forEach((coord) => {
    resLabel.innerText += `${coord[0]}, ${coord[1]} \n`;
  });
});
