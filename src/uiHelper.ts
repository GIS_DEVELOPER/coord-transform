export class UIHelper {
  private controlContainer: HTMLDivElement;

  constructor() {
    this.controlContainer = document.createElement("div");
    this.controlContainer.classList.add("control-container");
    document.body.appendChild(this.controlContainer);
  }

  public addBtn(name: string) {
    let btn = document.createElement("button");
    btn.innerText = name;
    this.controlContainer.appendChild(btn);
    return btn;
  }

  public addInput() {
    let input = document.createElement("input");
    this.controlContainer.appendChild(input);
    return input;
  }

  public addBr(){
     let div = document.createElement('div');
     div.style.height = "5px";
     this.controlContainer.appendChild(div);
     return div;
  }

  public addLabel(){
    let label = document.createElement('label');
    this.controlContainer.appendChild(label);
    return label;
  }
}
