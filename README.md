# LocalCoords

实现国内地方投影坐标，Beijing54 地理坐标,Beijing54 投影坐标，WGS84 地理坐标的转换

注：投影坐标即为平面坐标，用 xyz 表示，地理坐标为球面坐标，用经纬度表示

## 安装

```
npm i localcoords
```

## 使用

**地方投影坐标 <=> Beijing54 投影坐标**

国内的投影坐标一般基于 Beijing54 坐标系，所以两者之间的转换需要通过四参数拟合得到

```javascript
import {localproj_bjproj} from 'localcoords'

let fourParams = {
    x_0: 3109444.007744, // x轴偏转
    y_0: 557065.040376, // y轴偏转
    t: -1.188967233, // 旋转因子
    k: 0.985971406835, // 比例常数
}

let coord:[number,number] | Array<[number, number]> = [328443.4399,345557.6179];

// 正向转换
let resCoord1 = localproj_bjproj(coord, fourParams);

// 逆向转换
let resCoord2 = localproj_bjproj(coord, fourParams, true);

```

**Beijing54 投影坐标系 <=> Beijing54 地理坐标系**

地理坐标和投影坐标的转化需要设置投影参数，国内大比例尺地图通常都是高斯投影，所以投影参数中常用的两个参数就是中央经线和东偏距离

```javascript
import {bjproj_bjlnglat} from 'localcoords';

let coord: [number,number] | Array<[number, number]> = [3546276.946407,383516.597035];

// 正向转换
let res1 = bjproj_bjlnglat(coord, {
    lon_0: 120, // 中央经线
    x_0: 500000, // 东偏
});

// 逆向转换
let res1 = bjproj_bjlnglat(coord, {
    lon_0: 120,
    x_0: 500000,
}, true);

```

**Beijing54地理坐标系 <=> WGS84地理坐标系**

 Beijing54和WGS84采用不同的椭球基准，常用的拟合方法为七参数法，这里可以直接使用proj4的库进行转化

 ```javascript
 import {bjlnglat_wgslnglat} from 'localcoords';
 
 let coord: [number, number] | Array<number, number> = [118.766898215,32.033336978 ];

 // 正向转换
 let res1 = bjlnglat_wgslnglat(coord);
 // 逆向转换
 let res2 = bjlnglat_wgslnglat(coord, true);
 ```

## 其他

[源码](https://gitee.com/GIS_DEVELOPER/coord-transform)