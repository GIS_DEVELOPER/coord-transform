import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import typescript from '@rollup/plugin-typescript';
// `npm run build` -> `production` is true
// `npm run dev` -> `production` is false
const production = !process.env.ROLLUP_WATCH;

export default {
	input: production? 'src/projHelper.ts':'src/test.ts',
	output: {
		file: 'public/bundle.js',
		format: 'umd', // immediately-invoked function expression — suitable for <script> tags
		name: 'localcoords',
		sourcemap: true
	},
	plugins: [
		resolve({
			extensions: ['js', 'ts']
		}), // tells Rollup how to find date-fns in node_modules
		typescript({
			exclude: ["node_modules"],
			allowSyntheticDefaultImports: true,
			allowJs: true
		}),
		commonjs({
			extensions: ['js', 'ts']
		}), // converts date-fns to ES modules
		production && terser() // minify, but only in production
	]
};
